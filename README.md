# Otus.Teaching.PromoCodeFactory

Домашнее задание №3 Булатова Кирилла Павловича `C# ASP.NET Core Разработчик`/`Отус`.
Cистема `Promocode Factory` для выдачи промокодов партнеров для клиентов по группам предпочтений(`Отус`).

Данный проект является результатом для Homework №3

Подробное описание проекта можно найти в [Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Home)

Описание домашнего задания в [Homework Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Homework-3)